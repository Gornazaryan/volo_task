import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HomeModule } from 'app/home/home.module';
import { NgModel, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


import { EmailValidationDirective, PasswordValidationDirective } from './_directives/index';

import { AuthService, UsersService } from './_services/index';

import { AuthGuard } from 'app/_services/auth.guard';

import { AppRoutes } from 'app/_services/appRoutes';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmailValidationDirective,
    PasswordValidationDirective,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HomeModule,
    RouterModule.forRoot(AppRoutes),
  ],
  providers: [ AuthService, HomeModule, AuthGuard, UsersService],
  bootstrap: [AppComponent],
  exports: [ RouterModule ]
})
export class AppModule { }
