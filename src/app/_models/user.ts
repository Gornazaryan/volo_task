export class User {
    id: number;
    first_name: string;
    last_name: string;
    avatar: string
    createdAt: number
    updatedAt: number


    constructor( id: number = 0, first_name: string = '',
                last_name: string = '', avatar: string= 'http://lorempixel.com/200/200/people/?',
                createdAt?: number, updatedAt?: number ) {

        this.id = id
        this.first_name = first_name,
        this.last_name = last_name,
        this.avatar = avatar
        this.createdAt = createdAt
        this.updatedAt = updatedAt
    }

}
