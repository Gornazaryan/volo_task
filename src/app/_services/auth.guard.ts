import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from 'app/_services';

@Injectable()

export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {}

    canActivate (route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        const timeNow = new Date().getTime()
          console.log('you have allready loged in for ', timeNow - this.authService.loggedInTime, 'milliseconds')
          // expires token after 1 hour
        if (this.authService.token && timeNow - this.authService.loggedInTime < 3600000 ) {
          return true
        }
        this.authService.logOut()
        return false
        }
}
