import { Injectable } from '@angular/core';
import { User } from 'app/_models';
import { Observable } from 'rxjs/Rx';
import { Http } from '@angular/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { AuthService } from 'app/_services/auth.service';
import { LoaderService } from 'app/_services/loader.service';


@Injectable()
export class UsersService {
  pageURL= 'https://reqres.in/api/users?page=';
  userURL = 'https://reqres.in/api/users/'
  selectedUser: User = new User ()
  currentPage = 1

  constructor( private http: Http, private authService: AuthService, private loader: LoaderService) {}

  getUsers(page: number): Observable < any > {
    this.loader.openLoader()
    const header = new Headers({ 'Authorization': this.authService.token });
    const options = new RequestOptions({ headers: header });

    return this.http.get(this.pageURL + page, options)
      .map((resp: Response) => {
        return resp.json()
      })
      .catch((error: any) => {
        return Observable.throw(error);
      })
    .finally(() => this.loader.closeLoader() )
  }

  addUser(newUser: User): Observable < any >  {
    this.loader.openLoader()
    const body = JSON.stringify(newUser);
    const headers = new Headers({
      'Authorization': this.authService.token,
      'Content-Type': 'application/json;charset=utf-8',
    });
    const options = new RequestOptions({ headers: headers, body: body });

    return this.http.post( this.userURL, options)
      .map((resp: Response) => {
        const userAddedBody = JSON.parse(resp.json().body)
        const id = resp.json().id
        const createdAt = resp.json().createdAt
        const user = new User(id, userAddedBody.first_name, userAddedBody.last_name, userAddedBody.avatar + id, createdAt)
        return user
      })
      .catch((error: any) => {
        return Observable.throw(error);
      })
      .finally(() => this.loader.closeLoader())
  }

  delUser(id: number): Observable < any >  {
    this.loader.openLoader()
    const headers = new Headers({
      'Authorization': this.authService.token,
      'Content-Type': 'application/json;charset=utf-8',
    });
    const options = new RequestOptions({ headers: headers });

    return this.http.delete(this.userURL + id, options )
      .map((resp: Response) => resp.json())
      .catch((error: any) => {
        return Observable.throw(error);
      })
      .finally(() => this.loader.closeLoader())
  }

  editUser(editedUser: User): Observable < any >  {
    this.loader.openLoader()
    const body = JSON.stringify(editedUser);
    const headers = new Headers({
      'Authorization': this.authService.token,
      'Content-Type': 'application/json;charset=utf-8',
    });
    const options = new RequestOptions({ headers: headers, body: body });

    return this.http.put(this.userURL + editedUser.id, options)
      .map((resp: Response) => {
        return resp.json()
      })
      .catch((error: any) => {
        return Observable.throw(error);
      })
      .finally(() => this.loader.closeLoader())
  }

  getSingleUser(id: number): Observable < any > {
    this.loader.openLoader()
    const header = new Headers({ 'Authorization': this.authService.token });
    const options = new RequestOptions({ headers: header });

    return this.http.get(this.userURL + id, options)
      .map((resp: Response) => {
        return resp.json()
      })
      .catch((error: any) => {
        return Observable.throw(error);
      })
    .finally(() => this.loader.closeLoader() )
  }

}
