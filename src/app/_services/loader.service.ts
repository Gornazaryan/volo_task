import { Injectable } from '@angular/core';

@Injectable()
export class LoaderService {

  loaded: Boolean = false

  constructor() { }

  openLoader(): void {
    this.loaded = true
  }

  closeLoader(): void {
    this.loaded = false
  }

}
