import { Injectable } from '@angular/core';
import { User } from 'app/_models';

@Injectable()
export class SortingService {

  constructor() { }

  sortBy(arr: User[], byWhat: string): User[] {
    arr.sort(
      (a, b) => {
        if (a[byWhat] < b[byWhat]) {
          return -1
        }
        if (a[byWhat] > b[byWhat]) {
          return 1
        }
        return 0;
      })
    return arr
  }
}
