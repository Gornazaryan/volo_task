import { Injectable } from '@angular/core';
import { User, LoginUser } from 'app/_models';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { UsersService } from 'app/_services/users.service';
import { Observable } from 'rxjs/Observable';
import { Response, Headers } from '@angular/http';
import { LoaderService } from 'app/_services/loader.service';

@Injectable()
export class AuthService {

  loginURL = 'https://reqres.in/api/login/';

  constructor(private route: Router, private http: Http, private loader: LoaderService ) {
  }

  login(obj: LoginUser): Observable<any> {
    this.loader.openLoader()
    const body = JSON.stringify(obj);
    const headers = new Headers({
      'Content-Type': 'application/json;charset=utf-8'
    });

    return this.http.post(this.loginURL, body, {
        headers: headers
      })
      .map((resp: Response): boolean => {
        const token = resp.json().token;
        const email: string = obj.email;
        const loggedInTime = new Date().getTime()
        if (token) {
            localStorage.setItem('currentUser', JSON.stringify({ email: email, token: token, loggedInTime: loggedInTime }));
            return true;
        } else {
            return false;
        }
      })
      .catch((error: any) => {
        return Observable.throw(error)
      })
      .finally(() => {
        this.loader.closeLoader()
      })
  }

  logOut() {
    localStorage.removeItem('currentUser');
    this.route.navigate(['login'])
  }

  get token(): string {
    if (localStorage.currentUser) {
      return JSON.parse(localStorage.currentUser).token
    }
  }

  get loggedInTime(): number {
    if (localStorage.currentUser) {
      return JSON.parse(localStorage.currentUser).loggedInTime
    }
  }

}
