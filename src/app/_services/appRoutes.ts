import { Routes } from '@angular/router';

import { LoginComponent } from 'app/login/login.component';
import { HomeComponent } from 'app/home/home.component';
import { UsersListComponent } from 'app/home/users-list/users-list.component';
import { UserEditComponent } from 'app/home/user-edit/user-edit.component';
import { PageNotFoundComponent } from 'app/page-not-found/page-not-found.component';

import { AuthGuard } from 'app/_services/auth.guard';


export const AppRoutes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full'},
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'users', pathMatch: 'full'},
            { path: 'users', component: UsersListComponent},
            { path: 'edituser/:id', component: UserEditComponent}
        ]
    },
    { path: '**', component: PageNotFoundComponent}
]
