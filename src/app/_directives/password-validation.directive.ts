import { Directive} from '@angular/core';
import { AbstractControl, Validator, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[appPasswordValidation]',
    providers: [{provide: NG_VALIDATORS, useExisting: PasswordValidationDirective, multi: true}]
  })
  export class PasswordValidationDirective implements Validator {

    rexp: RegExp= /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/

    validate(control: AbstractControl): ValidationErrors {
      return this.rexp.test(control.value) ? null : {'Invalid Password': {value: control.value}};
    }
  }
