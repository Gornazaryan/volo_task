import { Directive} from '@angular/core';
import { AbstractControl, Validator, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[appEmailValidation]',
    providers: [{provide: NG_VALIDATORS, useExisting: EmailValidationDirective, multi: true}]
  })
  export class EmailValidationDirective implements Validator {

    rexp: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    validate(control: AbstractControl): ValidationErrors {
      return this.rexp.test(control.value) ? null : {'Invalid Email': {value: control.value}};
    }
  }
