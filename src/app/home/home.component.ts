import { Component, OnInit } from '@angular/core';
import { UsersService, AuthService } from 'app/_services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  email: string

  constructor( private authService: AuthService ) { }


  ngOnInit(): void {
    this.email = JSON.parse(localStorage.currentUser).email
  }

}
