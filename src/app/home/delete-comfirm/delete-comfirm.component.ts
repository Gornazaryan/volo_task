import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UsersListComponent } from 'app/home/users-list/users-list.component';
import { User } from 'app/_models';
import { UsersService } from 'app/_services';

declare var $: any;

@Component({
  selector: 'app-delete-comfirm',
  templateUrl: './delete-comfirm.component.html',
  styleUrls: ['./delete-comfirm.component.css']
})
export class DeleteComfirmComponent implements OnInit {

  @Output() deleted: EventEmitter<string> = new EventEmitter()

  constructor( private usersService: UsersService ) {}

  delete(): void {
    this.usersService.delUser(this.usersService.selectedUser.id)
      .subscribe(() => {
        console.log(this.usersService.selectedUser)
        this.deleted.emit('User deleted and current page has been updated')
      }
      )
  }

  ngOnInit(): void {  }
}
