import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MdProgressBarModule } from '@angular/material';


import { HomeComponent } from './home.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { DeleteComfirmComponent } from './delete-comfirm/delete-comfirm.component';
import { AddUserComponent } from './add-user/add-user.component';
import { LoadingBarComponent } from 'app/loading-bar/loading-bar.component';
import { AlertComponent } from './alert/alert.component';


import { SortingService } from 'app/_services';
import { LoaderService } from 'app/_services/loader.service';


import { NameFromEmailPipe } from './_pipes/name-from-email.pipe';
import { CapitalizeFirstLettersPipe } from './_pipes/capitalize-first-letters.pipe';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MdProgressBarModule,
  ],
  declarations: [
    HomeComponent,
    UsersListComponent,
    UserEditComponent,
    LoadingBarComponent,
    AddUserComponent,
    DeleteComfirmComponent,
    NameFromEmailPipe,
    CapitalizeFirstLettersPipe,
    AlertComponent ],
  exports: [ RouterModule, LoadingBarComponent ],
  providers: [ SortingService, LoaderService ],
})
export class HomeModule { }

