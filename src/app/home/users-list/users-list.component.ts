import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { UsersService, SortingService, AuthService } from 'app/_services';
import { User } from 'app/_models';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from 'app/_services/loader.service';

declare var $: any;

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})

export class UsersListComponent implements OnInit {

  newUser: User
  usersOnEachPage: User[]
  pages: number[]

  constructor( private sort: SortingService,
    private route: Router, private usersService: UsersService,  private loaderService: LoaderService ) {}


  setUser(user): void {
    this.usersService.selectedUser = user
  }

  sortBy(byWhat, event): void {
    this.usersOnEachPage = this.sort.sortBy(this.usersOnEachPage, byWhat)
    $('td').removeClass('sorted');
    $(event.target).parent().addClass('sorted');
    if ( $(event.target).hasClass('fa-sort-asc') ) {
      $(event.target).addClass('fa-sort-desc')
      $(event.target).removeClass('fa-sort-asc')
    }else {
      this.usersOnEachPage.reverse()
      $(event.target).addClass('fa-sort-asc')
      $(event.target).removeClass('fa-sort-desc')
    }
  }

  edit(user: User): void {
    this.route.navigate(['home/edituser', user.id]);

  }

  pageChanger(pageNumber: number, event: Event): void {
    this.usersOnEachPage = []
    this.usersService.currentPage = pageNumber
    this.updatePageData()
    $('.pageIcon').removeClass('active')
    $(event.target).addClass('active')
  }

  createPageArray(totalPages: number): void {
    this.pages = []
    for (let i = 1; i <= totalPages; i++) {
      this.pages.push(i)
    }
  }

  updatePageData(event: string= 'Page Loaded'): void {
      this.usersService.getUsers(this.usersService.currentPage).subscribe(
      (resp) => {
        this.usersOnEachPage = resp.data
        this.createPageArray(resp.total_pages)
        console.log(event)
      }
    )
  }

  ngOnInit(): void {
   this.updatePageData()
  }

}
