import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from 'app/_services';
import { Router } from '@angular/router';
import { User } from 'app/_models';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input() alertedUser: User = new User()
  @Input() alertText: string

  constructor( private router: Router ) { }

  goBack(): void {
    this.router.navigate(['home/users'])
  }

  ngOnInit(): void {
  }

}
