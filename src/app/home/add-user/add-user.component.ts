import { Component, OnInit, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'app/_models';
import { UsersService } from 'app/_services';
import { Router } from '@angular/router';
import { UsersListComponent } from 'app/home/users-list/users-list.component';

declare var $: any;

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})

export class AddUserComponent implements OnInit {

  @Output()
  added: EventEmitter<string> = new EventEmitter();
  alertText: string
  newUser: User = new User()
  addedUser: User = new User()


  constructor(private usersService: UsersService ) {}

  save(): void {
    this.usersService.addUser(this.newUser)
      .subscribe(
        (resp) => {
          this.addedUser = resp
          console.log(this.addedUser)
          this.alertText = 'User added in Fake API as'
          $('#AlertModal').modal('show')
          this.reset()
          this.added.emit('User added and current page has been updated');
        }
      )
  }

  reset(): void {
    $('form')[0].reset()
  }

  ngOnInit(): void {
  }

}
