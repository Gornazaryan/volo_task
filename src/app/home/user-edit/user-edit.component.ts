import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService, LoaderService } from 'app/_services';
import { User } from 'app/_models';

declare var $: any;

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  id: number
  editedUser: User = new User ()
  alertText: string


  constructor(private usersService: UsersService, private router: Router,
    private loaderService: LoaderService, private activeRoute: ActivatedRoute, ) { }

  cancel(): void {
    this.router.navigate([ 'home/users'])
  }

  save(): void {
    this.usersService.editUser(this.editedUser)
      .subscribe(
        (resp) => {
          const responseBody = JSON.parse(resp.body)
          // tslint:disable-next-line:max-line-length
          this.editedUser = new User(responseBody.id, responseBody.first_name, responseBody.last_name, responseBody.avatar, undefined,  resp.updatedAt)
          console.log(this.editedUser)
          this.alertText = 'User edited in Fake API to'
          $('#AlertModal').modal('show')
        }
      )
  }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.id = params.id
      this.usersService.getSingleUser(this.id)
        .subscribe(resp => {
          this.editedUser = new User (resp.data.id, resp.data.first_name, resp.data.last_name, resp.data.avatar)
        })
    })
  }

}
