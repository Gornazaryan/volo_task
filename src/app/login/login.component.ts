import { Component, OnInit, ElementRef} from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../_services/index';

import { User, LoginUser} from '../_models/index';
import { UsersService } from 'app/_services/users.service';
import { LoaderService } from 'app/_services/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [],
})

export class LoginComponent implements OnInit {

  username = '';
  password = '';

  constructor( private authService: AuthService , private router: Router,
    private loaderService: LoaderService ) {
  }

  login(): void {
    const loginObj: LoginUser = {
      'email': this.username,
      'password': this.password,
    }
    this.authService.login(loginObj)
      .subscribe(() => {
        this.router.navigate(['home'])
      } )
  }

  ngOnInit(): void {
    if (localStorage.currentUser) {
      this.router.navigate(['home'])
    }
  }
}
