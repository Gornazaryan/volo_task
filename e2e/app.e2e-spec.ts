import { VoloTaskPage } from './app.po';

describe('volo-task App', () => {
  let page: VoloTaskPage;

  beforeEach(() => {
    page = new VoloTaskPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
